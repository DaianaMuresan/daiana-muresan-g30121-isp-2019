package muresan.daiana.lab6.ex2;

public class TestBankAccount {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Daiana", 9000);
        bank.addAccount("Ioana", 8900);
        bank.addAccount("Andreea", 9000);
        bank.addAccount("Maria", 6770);
        bank.addAccount("Claudia", 8190);
        bank.addAccount("Elena", 9000);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(8000, 9000);
        System.out.println("Get Account by owner name");
        bank.getAccount("Daiana");
        System.out.println("Get All Account order by name");
        bank.getAllAccounts();
    }
}
