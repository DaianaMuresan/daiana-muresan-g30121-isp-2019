package muresan.daiana.lab6.ex1;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Daiana", 9000);
        BankAccount b2 = new BankAccount("Daiana", 8700);
        BankAccount b3 = new BankAccount("Andreea", 9000);
        if (b1.equals(b2))
            System.out.println(b1 + " and " + b2 + " are equals");
        else System.out.println(b1 + " and " + b2 + " are not equals");

        if (b1.equals(b3))
            System.out.println(b1 + " and " + b3 + " are equals");
        else System.out.println(b1 + " and " + b3 + " are not equals");

        if (b2.equals(b3))
            System.out.println(b2 + " and " + b3 + " are equals");
        else System.out.println(b2 + " and " + b3 + " are not equals");
        System.out.println(b1.hasCode());
        System.out.println(b2.hasCode());
        System.out.println(b3.hasCode());
    }
}
