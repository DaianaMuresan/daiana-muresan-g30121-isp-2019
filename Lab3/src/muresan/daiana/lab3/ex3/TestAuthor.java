package muresan.daiana.lab3.ex3;

public class TestAuthor {
    public static void main(String args[]){
        Author a1 = new Author("DaianaMuresan","muresandaiana@yahoo.com",'f');
        Author a2 = new Author("CiceuTiberiu","ciceutl@gmail.com",'m');
        Author a3 = new Author("BodocanSebi","bodos@hotmail.com",'m');
        System.out.println(a1.getName());
        System.out.println(a1.getEmail());
        System.out.println(a1.getGender());
        System.out.println(a2.getName());
        System.out.println(a2.getEmail());
        System.out.println(a2.getGender());
        System.out.println(a3.getName());
        System.out.println(a3.getEmail());
        System.out.println(a3.getGender());
        a1.toString();
        a1.setEmail("daiana_muresan@yahoo.com");
        System.out.println(a1.getEmail());
    }
}
