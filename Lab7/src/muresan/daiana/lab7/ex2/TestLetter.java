package muresan.daiana.lab7.ex2;

import java.nio.file.Path;
import java.nio.file.Paths;

public class TestLetter {
    public static void main(String[] args) {
        Path file = Paths.get("/Users/daianamuresan/Documents/JavaPrgms/Lab7/src/muresan/daiana/lab7/ex2/data.txt");
        CountLetter cl=new CountLetter(file,'a');
        CountLetter cl2=new CountLetter(file,'j');
        CountLetter cl3=new CountLetter(file,'h');
        System.out.println(cl.count());
        System.out.println(cl2.count());
        System.out.println(cl3.count());

    }
}
