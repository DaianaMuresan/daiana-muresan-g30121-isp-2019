package muresan.daiana.lab7.ex1;

public class CoffeeDrinker extends Exception {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, PredefinedException {
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Coffee is to hot!");
        if (c.getConc() > 50)
            throw new ConcentrationException(c.getConc(), "Coffee concentration to high!");
        System.out.println("Drink coffee:" + c);

        if (c.getPred() > 10) {
            throw  new PredefinedException(c.getPred(),"Too much coffee!");

        }


    }
}

