package muresan.daiana.lab1.ex3;

public class Vehicle {
    int speed;
    String color;
    public Vehicle(int speed, String color)
    {
        this.speed=speed;
        this.color=color;
    }
    public void move(int speed) {
        System.out.println("Vehicle moved with speed "+speed);
    }
    public void paint(String color) {
        System.out.println("Vehicle painted in "+color);
    }
}
