package muresan.daiana.lab5.ex1;

public class TestShapes {
    public static void main(String[] args) {
        Circle c = new Circle(6,"green",false);
        System.out.println(c.toString());
        System.out.println("Area = "+c.getArea());
        System.out.println("Perimeter = "+c.getPerimeter());
        System.out.println("Is filled = "+c.isFilled());

        Rectangle r = new Rectangle(8,3,"purple",false);
        System.out.println(r.toString());
        System.out.println("Width = "+r.getWidth());
        r.setWidth(10);
        System.out.println("New width = "+r.getWidth());
        System.out.println("Area = "+r.getArea());
        System.out.println("Is filled = "+r.isFilled());

        Square sq = new Square(7,"neon",false);
        sq.setSide(1);
        System.out.println(sq.toString());
        System.out.println("Is filled = "+sq.isFilled());
        System.out.println("Perimeter = "+sq.getPerimeter());

    }
}
