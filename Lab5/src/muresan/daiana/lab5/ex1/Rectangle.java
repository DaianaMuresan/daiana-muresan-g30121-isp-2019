package muresan.daiana.lab5.ex1;

public class Rectangle extends Shape{
    protected double width;
    protected double length;
    public Rectangle() {
        this.width = 1.0;
        this.length = 1.0;
    }
    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }
    public Rectangle(double width, double lenght, String color, boolean filled) {
        super (color,filled);
        this.width = width;
        this.length = lenght;
        this.color = color;
        this.filled = filled;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
    @Override
    public double getArea(){
        return this.width*this.length;
    }
    @Override
    public double getPerimeter(){
        return 2*(this.width+this.length);
    }
    @Override
    public String toString() {
        return "Rectangle with width= "+this.width+" and length= "+this.length+" which is a subclass of "+super.toString();
    }
}
