package muresan.daiana.lab5.ex1;

public class Circle extends Shape {
    protected double radius;

    public Circle() {
        this.radius = 1.0;
    }
    public Circle(double radius){
        this.radius = radius;
    }
    public Circle(double radius, String color, boolean filled){
        super(color,filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    @Override
    public double getArea(){
        return this.radius*this.radius*3.14;
    }
    @Override
    public double getPerimeter(){
        return 2*3.14*this.radius;
    }
    @Override
    public String toString(){
        return "A Circle with radius "+this.radius+" which is a subclass of "+ super.toString();
    }
}
