package muresan.daiana.lab2.ex3;
import java.util.Scanner;
public class PrimeNumbers {

    public static boolean isPrime(int n) {
        if((n == 1) || (n == 0)) {return false;}
        if (n%2==0) return false;
        for(int i=3;i*i<=n;i+=2) {
            if(n%i==0)
                return false;
        }
        return true;
    }
    public static void main(String args[]) {
        Scanner s = new Scanner(System.in);
        System.out.println("a=");
        int a = s.nextInt();
        System.out.println("b=");
        int b = s.nextInt();
        int number=0,count=0;
        for(number=a;number<=b;number++) {
            if (isPrime(number) == true) {
                count++;
                System.out.println(number + ", ");
            }
        }
        System.out.println("There are " + count + " prime numbers");
    }

}